#ifndef functions_H
#define functions_H
#include <iostream>

template <class T>
int compare(T t1, T t2)
{
	if (t1 > t2)
	{
		return -1;
	}
	else if (t1 == t2)
	{
		return 0;
	}
	return 1;
}

template <class T>
void swap(T &t1, T &t2)
{
	T temp = t1;
	t1 = t2;
	t2 = temp;
}

template <class T>
void bubbleSort(T arr[], int n)
{
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (compare<T> (arr[j], arr[j+1]) == -1)
			{
				swap<T>(arr[j], arr[j + 1]);
			}
}

template <class T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++)
		std::cout << arr[i] << std::endl;
}
#endif