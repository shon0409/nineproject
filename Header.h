#ifndef BSNode_H
#define BSNode_H

using namespace std;

template<typename T>
class BSNode
{
public:
	BSNode<T>();

	BSNode<T>(T data) {
		this->_data = data;
		this->_left = nullptr;
		this->_right = nullptr;
		this->_count = 1;
	}
	BSNode<T>(const BSNode<T>& other) {
		this->_count = other._count;
		this->_data = string(other._data);
		if (other._right)
		{
			this->_right = new BSNode<T>(*other._right);
		}
		else
		{
			this->_right = nullptr;
		}
		if (other._left)
		{
			this->_left = new BSNode<T>(*other._left);
		}
		else
		{
			this->_left = nullptr;
		}
	}

	~BSNode<T>() {
		if (this->_right)
		{
			delete this->_right;
		}
		if (this->_left)
		{
			delete this->_left;
		}
	}

	void insert(T value) {
		if (value == this->_data)
		{
		this->_count++;
		}
		else
		{
			if (value > this->_data)
			{
				if (this->_right)
				{
					this->_right->insert(value);
				}
				else
				{
					this->_right = new BSNode<T>(value);
				}
			}
			else
			{
				if (this->_left)
				{
					this->_left->insert(value);
				}
				else
				{
					this->_left = new BSNode<T>(value);
				}
			}
		}
	}
	BSNode<T>& operator=(const BSNode<T>& other)
	{
		this->_count = other.getCount();
		this->_data = string(other.getData());
		if (other._right)
		{
			this->_right = new BSNode<T>(*other.getRight());
		}
		else
		{
			this->_right = nullptr;
		}
		if (other._left)
		{
			this->_left = new BSNode<T>(*other.getLeft());
		}
		else
		{
			this->_left = nullptr;
		}

		return *this;
	}

	bool isLeaf() const
	{
		return (this->_right == nullptr && this->_left == nullptr);
	}

	T getData() const
	{
		return T(this->_data);
	}
	int getCount() const
	{
		return this->_count;
	}
	BSNode<T>* getLeft() const
	{
		return this->_left;
	}
	BSNode<T>* getRight() const
	{
		return this->_right;
	}

	bool search(T val) const
	{
		bool ans = false;
		if (val == this->_data)
		{
			ans = true;
		}
		else if (!this->isLeaf()) //if it is a leaf, ans will stay false
		{
			if (getRight())
			{
				ans = this->getRight()->search(val);
			}
			if (getLeft())
			{
				ans = ans || this->getLeft()->search(val);
			}
		}
		return ans;
	}

	int getHeight() const
	{
		int height = 0, rightHeight = 0, leftHeight = 0;
		if (!isLeaf())
		{
			if (getLeft())
			{
				leftHeight = 1 + getLeft()->getHeight();
			}
			if (getRight())
			{
				rightHeight = 1 + getRight()->getHeight();
			}
			height = std::_Max_value(leftHeight, rightHeight);
		}
		return height;
	}

	int getDepth(const BSNode<T>& root) const
	{
		int depth = -1;
		if (root.search(this->getData()))
		{
			depth = getCurrNodeDistFromInputNode(&root);
		}
		return depth;
	}

	void printNodes() const
	{
		if (_left)
		{
			_left->printNodes();
		}
		cout << this->_data << " " << this->_count << endl;
		if (_right)
		{
			_right->printNodes();
		}
	}

	T& operator [](const int index)
	{
		if (index >= 0 && index < ARRAY_MAX_SIZE)
			return items[index];
		else
		{
			cerr << "INDEX OUT OF BOUNDS!!!";
			exit(1);
		}
	}

private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;



	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<T>* node) const {
		int depth = 0;
		if (!(this->getData() == node->getData()))
		{
			if (node->getRight() != nullptr && node->getRight()->search(this->getData()))
			{
				depth = 1 + getCurrNodeDistFromInputNode(node->getRight());
			}
			else
			{
				depth = 1 + getCurrNodeDistFromInputNode(node->getLeft());
			}
		}
		return depth;
	}

};

#endif

template<typename T>
inline BSNode<T>::BSNode()
{
}
