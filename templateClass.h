#ifndef templateClass_H
#define templateClass_H

class MyClass
{
public:
	int var;
	MyClass(int val) {
		var = val;
	}

	bool operator>(MyClass other) {
		return var > other.var;
	}

	bool operator==(MyClass other) {
		return var == other.var;
	}
};

std::ostream& operator<<(std::ostream& out, const MyClass& mc)
{
	return out <<  mc.var;
}

#endif
